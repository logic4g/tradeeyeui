var app = angular.module('TradeEye', ['ngGrid', 'ui.bootstrap', 'ngRoute', 'ngMessages','ngResource']);


app.config(['$routeProvider', function($routeProvider) {

    $routeProvider
        .when('/login', {
            controller: 'LoginController',
            templateUrl: './views/login.html'
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: './views/register.html'
        })
        .when('/afterlogin', {
            controller: 'TabsCtrl',
            templateUrl: './views/tabs.html'
        })

        .otherwise({ redirectTo: '/login' });
}]);
