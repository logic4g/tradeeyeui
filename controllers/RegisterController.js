//app.controller('RegisterController',
//    ['$scope',
//        function ($scope) {
//
//        }]);

app.factory("RegisterService", function($http) {
    return {
        registerDetails: function(name,pass,cpass) {
            return $http.post('http://104.130.171.4/register/?username='+name+'&password='+pass+'&confirmPassword='+cpass).then(function(result) {
                return result.data;
            });
        }
    }
});


app.controller('RegisterController', function (RegisterService) {
    var model = this;

    model.message = "";

    model.user = {
        username: "",
        password: "",
        confirmPassword: ""
    };
    console.log("came here");
    model.submit = function(isValid) {
        console.log("Entered validity");
        if (isValid) {
           var name=model.user.username,
            pass=model.user.password,
            cpass=model.user.confirmPassword;

            RegisterService.registerDetails(name,pass,cpass).then(function(data) {
                console.log(data);
            });

            model.message = "Submitted" + model.user.username;
        } else {
            model.message = "Please check the fields with red color";
        }
    };

});

app.directive('compareTo', function(){
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});





