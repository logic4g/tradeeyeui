/**
 * Created by keerthana.gotur on 12/29/2014.
 */
app.controller('TabsCtrl', ['$scope',
    function($scope) {
        $scope.tabs = [{
            title: 'Dashboard',
            url: './views/dashboard.html'
        },{
            title: 'Admin',
            url: './views/admin.html'
        },{
            title: 'Daily Alerts',
            url: './views/dailyalerts.html'
        }, {
            title: 'Case Management',
            url: './views/casemgmt.html'
        }];

        $scope.currentTab = './views/dailyalerts.html';

        $scope.onClickTab = function(tab) {
            $scope.currentTab = tab.url;
        }

        $scope.isActiveTab = function(tabUrl) {
            return tabUrl == $scope.currentTab;
        }
    }
]);


//app.config(['$resourceProvider', function($resourceProvider) {
//    // Don't strip trailing slashes from calculated URLs
//    $resourceProvider.defaults.stripTrailingSlashes = false;
//}]);


app.factory('alertService', function($http) {
    return {
        getAlerts: function() {
            return $http.get('http://104.130.171.4:7777').then(function(result) {
                return result.data;
            });
        }
    }
});


app.factory("PopUpService", function($http) {
//    return $resource("http://192.168.1.107/trade/id/:id", {}, {
//        query: { method: "GET" }
//    });
    return {
        fullTradeDetails: function(id) {
            return $http.get('http://192.168.1.107/trade/id/'+id).then(function(result) {
                return result.data;
            });
        }
    }
});




app.filter('numberFilter', function($filter) {
    return function(input) {
        var originalFilter = $filter('number');
        return input == null ? '' : originalFilter(input);
    };
});
//var cellTemplate='<div class="ngCellText"  data-ng-model="row"><button data-ng-click="popUpTable(row,$event)">viewprofile</button></div>'
app.controller('DailyAlertCtrl', function($scope, $modal, $http, alertService) {


    //Gets json from url
//    $http.get('http://104.130.171.4:7777').success(function (data) {
//
//        $scope.myData = data;
//    });
    alertService.getAlerts().then(function(data) {
        //this will execute when the
        //AJAX call completes.
        $scope.myData = data;
    });

    //changes starts from here
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 40, 60],
        pageSize: 20,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length;
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('http://104.130.171.4:7777').success(function (largeLoad) {
                    data = largeLoad.filter(function(item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });
                    $scope.setPagingData(data,page,pageSize);
                });
            } else {
                $http.get('http://104.130.171.4:7777').success(function (largeLoad) {
                    $scope.setPagingData(largeLoad,page,pageSize);
                });
            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    //ends here


    $scope.gridOptions = {
        data: 'myData',
        enableCellSelection: true,
        multiSelect: false,
        enableRowSelection: false,
        columnDefs: [{field: 'tradeid', displayName: 'TradeID', cellTemplate: '<span ng-click="popUpTable(row,$event)" ng-cell-text>{{row.getProperty(col.field)}}</span>'},
            {field:'tradedate', displayName:'TradeDate'},
            // {field:'alertid', displayName:'Alert ID'},
            {field:'strikeprice', displayName:'Price'},
            {field:'trace', displayName:'Region/Code'},
            {field:'alertcategory', displayName:'AlertCategory'},
            {field:'instrumentid', displayName:'InstrumentID'},
            {field:'notionalamt', displayName:'NotionalAmt'},
            {field:'notionalamtcurrency', displayName:'NotionalAmtCurrency'}
            // {field:'risktype', displayName:'RiskType'},
            //{field:'ExceptionType', displayName:'ExceptionType'}
            //{field:'', cellTemplate:cellTemplate}
        ],
        enablePaging: true,
        showFooter: true,
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        plugins: [new ngGridCsvExportPlugin()],
        enableColumnResize: true
    };


    var dialog;
    $scope.popUpTable=function(row){
        $scope.myrow=row.entity;
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: ModalInstanceCtrl,
            resolve: {
                items: function () {
                    return row.entity;
                }
            }
        });
    }
});

var ModalInstanceCtrl = function ($scope, $modalInstance, $http, items, PopUpService) {

    $scope.items = items;
    var tradeid=items.tradeid;
    console.log(tradeid);
    PopUpService.fullTradeDetails(tradeid).then(function(data) {
        console.log(data);
    });

//    PopUpService.query({id: tradeid},function(data) {
//        $scope.posts = data;
//    });
//    $http.get('http://192.168.1.107/trade/id/:tradeid').success(function (data) {
//        $scope.posts=data.posts;
//    });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};


app.controller('CasemtCtrl', function($scope, $modal, $http, alertService) {

    $scope.statuses = ['Explain', 'Assign', 'Approve', 'Open', 'Close'];

    //Gets json from url
//    $http.get('http://104.130.171.4:7777').success(function (data) {
//
//        $scope.myData = data;
//    });

    alertService.getAlerts().then(function(data) {
        //this will execute when the
        //AJAX call completes.
        $scope.myData = data;
    });



    //changes starts from here
    $scope.filterOptions = {
        filterText: "",
        useExternalFilter: true
    };
    $scope.totalServerItems = 0;
    $scope.pagingOptions = {
        pageSizes: [20, 40, 60],
        pageSize: 20,
        currentPage: 1
    };
    $scope.setPagingData = function(data, page, pageSize){
        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
        $scope.myData = pagedData;
        $scope.totalServerItems = data.length
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    };
    $scope.getPagedDataAsync = function (pageSize, page, searchText) {
        setTimeout(function () {
            var data;
            if (searchText) {
                var ft = searchText.toLowerCase();
                $http.get('http://104.130.171.4:7777').success(function (largeLoad) {
                    data = largeLoad.filter(function(item) {
                        return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                    });
                    $scope.setPagingData(data,page,pageSize);
                });
            } else {
                $http.get('http://104.130.171.4:7777').success(function (largeLoad) {
                    $scope.setPagingData(largeLoad,page,pageSize);
                });
            }
        }, 100);
    };

    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

    $scope.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    $scope.$watch('filterOptions', function (newVal, oldVal) {
        if (newVal !== oldVal) {
            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);
        }
    }, true);
    //ends here
    $scope.selectedOption = $scope.statuses[1];
    $scope.gridOptions = {
        data: 'myData',
        multiSelect: false,
        columnDefs: [
            // {field: 'active', displayName: ' ', cellTemplate: '<div class="ngCellText" ng-cell-text ng-class="col.colIndex()"><input type="checkbox" ng-model="row.entity.active"><span ng-show="COL_FIELD" class=" glyphicon glyphicon-th-list" /></div>' },
            {field: 'caseid', displayName: 'Case ID', cellFilter:'numberFilter'},
            {field: 'name', displayName: 'Trader'},
            {field: 'tradeid', displayName: 'TradeID'},
            {field: 'tradedate', displayName: 'Trade Date'},
            {field: 'controlname', displayName: 'Control Name'},
            {field: 'transtype', displayName: 'Transaction Type'},
            {field: 'Assign', displayName: 'Assigned To'},
            {field: 'severity', displayName: 'Severity', cellTemplate: '<div style="color:{{row.entity.severity}}" ><div class="ngCellText">{{row.getProperty(col.field)}}</div></div>'},
            {field: 'status', displayName: 'Status'},
            {field: 'action', displayName: 'Action', cellTemplate: '<select ng-model="COL_FIELD"  ng-click="AssignPopUp(row,$event)"><option ng-repeat="status in statuses">{{status}}</option>{{row.getProperty(col.field)}}</select>'}
        ],
        enablePaging: true,
        showFooter: true,
        totalServerItems:'totalServerItems',
        pagingOptions: $scope.pagingOptions,
        filterOptions: $scope.filterOptions,
        plugins: [new ngGridCsvExportPlugin()],
        enableColumnResize: true
    };


    var dialog;
    $scope.AssignPopUp=function(row){
        $scope.myrow=row.entity;
        var modalInstance = $modal.open({
            templateUrl: 'selectassign.html',
            controller: SampleInstanceCtrl,
            resolve: {
                items: function () {
                    return row.entity;
                }
            }
        });
    }
});
var SampleInstanceCtrl = function ($scope, $modalInstance, items) {

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};


app.controller('RegionCtrl', ['$scope',
    function($scope) {
        $scope.region = [{
            name: 'AMER'
        }, {
            name: 'EMEA'
        }, {
            name: 'APAC'
        }]
    }
]);

app.controller('AssetclassCtrl', ['$scope',
    function($scope) {
        $scope.assetclass = [{
            name: 'Credit_Bond'
        }, {
            name: 'Credit_Loan'
        }, {
            name: 'Credit_CDS/CDX'
        }]
    }
]);

app.controller('TradingdeskCtrl', ['$scope',
    function($scope) {
        $scope.tradingdesk = [{
            name: 'MKTCORR'
        }, {
            name: 'CORPB'
        }]
    }
]);


app.controller('JiraCtrl', function($scope, $http) {
    $scope.issues = [];
    $http.get('http://localhost:9098').success(function(data) {
        $scope.data = data;
        console.log(data);
    });
});