app.factory('RestfulService', ['$resource', function($resource) {
    return $resource('http://192.168.1.107/runrules/', null,
        {
            'submit': { method:'GET'}
        });
}]);

app.controller('adminCtrl', ['$scope','$sce','$http','RestfulService' ,function($scope,$sce,$http,RestfulService) {
    $scope.products = ['bondoption','fx'];
    $scope.ruleCategory = ['tradeMandate'];
    $scope.ruleName = 'name';
    $scope.date = 'date';
    $scope.sparkView = function() {

        $scope.detailFrame= $sce.trustAsResourceUrl("http://104.130.171.4:8080");
        $http.get($sce.trustAsResourceUrl("http://104.130.171.4:8080")).success(function(data, status) {
                $scope.player = sce.trustAsHtml(data.html);
            })
    }
    $scope.runrules = function(product,rcategory,rname,businessdate) {
        RestfulService.submit({product:product,category:rcategory,name:rname,businessdate:businessdate});
        $scope.sparkView();
    }

}]);
