/**
 * Created by keerthana.gotur on 12/2/2014.
 */
var jira = require('jira-api');
var express = require('express');
var app = express();
var cors = require("cors");
app.use(cors());
var options = {
    config: {
        "username": "karthik",
        "password": "karthik@123",
        "host": "logic4g.atlassian.net"
    },
    data: {
        fields: {
            project: {
                key: "TRADE"
            },
            priority: {
                name: "High"
            },
            summary: "Sample Issue",
            description: "A more elaborate decription of the issue",
            issuetype: {
                name: "Task"
            }
        }
    }
};
app.get('/', function(req, res) {
    jira.issue.post(options, function(result) {
        console.log(result);
        res.json(result);
    });
});
app.listen(9098);

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    next();
});

module.exports = app;